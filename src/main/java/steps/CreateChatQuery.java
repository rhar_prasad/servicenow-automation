package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateChatQuery {

	public static ChromeDriver driver;
	
	@Given("Launch the Chrome Browser")
	public void launch_the_Chrome_Browser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	   	driver = new ChromeDriver();
	}

	@Given("Maximise the Browser")
	public void maximise_the_Browser() {
	    driver.manage().window().maximize();
	}

	@Given("Set the Timeouts")
	public void set_the_Timeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Load URL and navigate to servicenow with valid credentials")
	public void load_URL_and_navigate_to_servicenow_with_valid_credentials() {
		 driver.get("https://dev77567.service-now.com");
	}

	@Given("Enter the UserName as (.*)")
	public void enter_the_UserName(String username) {
		driver.switchTo().frame("gsft_main");
		driver.findElementById("user_name").sendKeys(username);
	}

	@Given("Enter the Password as (.*)")
	public void enter_the_Password(String password) {
		driver.findElementById("user_password").sendKeys(password);
	}

	@When("User Click on Login Button")
	public void user_Click_on_Login_Button() {
		driver.findElement(By.xpath("//button[contains(@class,'pull-right btn')]")).click();
		driver.switchTo().defaultContent();
	}

	@Then("Verify Login Success")
	public void verify_Login_Success() {
		System.out.println("Login Successfully");
	}

	@Given("Go To My Work")
	public void go_To_My_Work() throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@placeholder='Filter navigator']")).sendKeys("My Work");
		driver.findElement(By.xpath("(//div[text()='My Work'])[1]")).click();
	}

	@Given("Go To New Link")
	public void go_To_New_Link() {
		driver.switchTo().frame("gsft_main");
	    driver.findElement(By.xpath("//button[text()='New']")).click();
	}

	@Given("Go To Chat Queue Entry")
	public void go_To_Chat_Queue_Entry() {
	    driver.findElement(By.linkText("Chat_queue_entry")).click();
	}
	
	@Given("Get ChatQuery No")
	public String Get_Chat_Query_No() {
	   WebElement text = driver.findElement(By.xpath("//input[@id='chat_queue_entry.number']"));
	   String value = text.getAttribute("value");
	   System.out.println(value);
	   return value;
	}

	@Given("Entering Short Mandatory fields (.*)")
	public void entering_Short_Mandatory_fields_as(String shortDesc) {
	   driver.findElement(By.xpath("//input[@data-type='pick_list']")).sendKeys(shortDesc);
	}

	@Given("Entering notes fields (.*)")
	public void entering_notes_fields_as(String notes) {
	    driver.findElement(By.xpath("//textarea[@data-type='glide_journal_input']")).sendKeys(notes);
	}

	@When("Click on Submit Button")
	public void click_on_Submit_Button() {
		driver.findElement(By.xpath("//button[@id ='sysverb_insert']")).click();
	}

	@Then("verify Click create")
	public void verify_Click_create() {
	   
	}
}

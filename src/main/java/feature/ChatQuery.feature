Feature: ServiceNow Application

Background:

	Given Enter the UserName as admin
 	And Enter the Password as India@123
 	When User Click on Login Button
 	Then Verify Login Success
	
	
Scenario Outline: TC001 Positive Flow - Create Chat queue entry

	
 	Given Go To My Work <filtername>
 	And Go To New Link
 	And Go To Chat Queue Entry
 	And Get ChatQuery No
 	And Entering notes fields <notes>
 	And Entering Short Mandatory fields <shortdesc>
 	When Click on Submit Button
	Then verify Click create
		
	Examples: 
		|filtername|shortdesc|notes|
		|My Work|Testing|Selenium|
		
	
	
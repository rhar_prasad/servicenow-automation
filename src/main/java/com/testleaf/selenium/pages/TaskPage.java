package com.testleaf.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaft.selenium.api.design.Locators;

import cucumber.api.java.en.Given;

public class TaskPage extends SeleniumBase{
	public TaskPage() {
		defaultContent();
		switchToFrame("gsft_main");
	}
	
	public CreateTicketPage ClickTicket() {
		click(locateElement(Locators.LINK_TEXT, "Ticket"));
		
		return new CreateTicketPage();		
	}
	
	
	@FindBy(how=How.LINK_TEXT, using="Chat_queue_entry")
	private WebElement eleClickMyChat;
	
	@Given("Go To Chat Queue Entry")
	public CreateChatQueryPage ClickChatQuery() {
		click(locateElement(Locators.LINK_TEXT, "Chat_queue_entry"));
		return new CreateChatQueryPage();		
	}

}

package com.testleaf.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.testleaft.selenium.api.design.Locators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.testleaf.selenium.api.base.SeleniumBase;

public class LoginPage extends SeleniumBase {
	
	@Given("Enter the UserName as (.*)")
	public LoginPage enterUsername(String data) {
		switchToFrame("gsft_main");
	//	getDriver().switchTo().frame("gsft_main");
		getDriver().findElementById("user_name").sendKeys(data);	
		clearAndType(locateElement(Locators.ID, "user_name"),data);
		return this;
	}
	
	@Given("Enter the Password as (.*)")
	public LoginPage enterPassword(String password) {
		getDriver().findElementById("user_password").sendKeys(password);
		clearAndType(locateElement(Locators.ID, "user_password"),password);
		return this;
	}

	@When("User Click on Login Button")
	public HomePage clickLogin() {		
		click(locateElement(Locators.XPATH, "//button[contains(@class,'pull-right btn')]"));
		System.out.println("Login Successfully");
		defaultContent();
		return new HomePage();
	}
	
	@FindBy(how =How.XPATH, using ="//input[@placeholder='Filter navigator']")
	private WebElement filter;
	
	@Then("Verify Login Success")
	public void verify_Login_Success() {
		//verifyDisplayed(filter);
	}
}

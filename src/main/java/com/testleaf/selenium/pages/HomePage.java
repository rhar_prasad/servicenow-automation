package com.testleaf.selenium.pages;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaft.selenium.api.design.Locators;

import cucumber.api.java.en.Given;
import pages.HomePagePF;

public class HomePage extends SeleniumBase{

	public HomePage( ) {
		
	}
	@Given("Go To My Work (.*)")
	public MyWorkPage GoToMyWork(String filtername) throws InterruptedException
	{
		enterFilterSearch(filtername);
		clickMyWork();
		return new MyWorkPage();
	}
	
	public HomePage enterFilterSearch(String data) {
		defaultContent();
		clearAndType(locateElement(Locators.XPATH, "//input[@placeholder='Filter navigator']"),data);
		return this;
	}
	
	public CreateIncidentPage clickCreateNew() {
		click(locateElement(Locators.XPATH, "(//div[text()='Create New'])[1]"));
		defaultContent();
		return new CreateIncidentPage();		
	}
	
	public CreateIncidentPage clickOpen() {
		click(locateElement(Locators.XPATH, "//div[contains(text(),'Open')][1]"));
		defaultContent();
		return new CreateIncidentPage();		
	}
	
	public MyWorkPage clickMyWork() {
		click(locateElement(Locators.XPATH, "(//div[text()='My Work'])[1]"));
		defaultContent();
		return new MyWorkPage();		
	}
}

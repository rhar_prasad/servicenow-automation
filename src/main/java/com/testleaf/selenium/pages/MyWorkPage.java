package com.testleaf.selenium.pages;

import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.testng.api.base.ProjectSpecificMethods;
import com.testleaft.selenium.api.design.Locators;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class MyWorkPage extends ProjectSpecificMethods{
    
	public MyWorkPage() {
		defaultContent();
		switchToFrame("gsft_main");
	}
	
	@Given("Go To New Link")
	public TaskPage ClickNew() {
		click(locateElement(Locators.XPATH, "//button[text()='New']"));
		defaultContent();
		return new TaskPage();		
	}
	
	public MyWorkPage ClickAll() {
		click(locateElement(Locators.XPATH, "//b[.='All']"));
		defaultContent();
		return this;		
	}
	
	
	public MyWorkPage enterTicketID(String data) {
		clearAndType(locateElement(Locators.XPATH, "(//input[@class='form-control'])[1]"),data, Keys.ENTER);
		return this;
	}

	public UpdateTicketPage ClickTicketID(String data) {
		
		click(locateElement(Locators.XPATH, "//a[text()='"+data+"']"));
		defaultContent();
		return new UpdateTicketPage();
	}
	
	public MyWorkPage VerifyPriority(String ticketNumber, String PriorityStatus) {
		String ActualPriority = getElementText(locateElement(Locators.XPATH, "//a[text()='"+ticketNumber+"']/following::td[1]"));
		Assert.assertEquals(ActualPriority, PriorityStatus);
		
		return this;
	}

	public MyWorkPage VerifyState(String ticketNumber, String State) {
		String ActualState = getElementText(locateElement(Locators.XPATH, "//a[text()='"+ticketNumber+"']/following::td[2]"));
		Assert.assertEquals(ActualState, State);
		return this;
	}
	
	@Then("verify Click create")
	public MyWorkPage VerifyCreated()
	{
		enterTicketID(chatQueryNumber);
		verifyDisplayed(locateElement(Locators.XPATH, "//a[text()='"+chatQueryNumber+"']"));
		return this;
	}
}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.selenium.pages.MyWorkPage;

import cucumber.api.java.en.Given;

public class HomePagePF extends SeleniumBase {

	public HomePagePF() {
		PageFactory.initElements(getDriver(), this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@placeholder='Filter navigator']") 
	private WebElement eleFiltername;
	
	public HomePagePF enterFilterSearch(String filtername) throws InterruptedException {
		Thread.sleep(2000);
		clearAndType(eleFiltername, "My Work");
		return this;		
	}
	
	@Given("Go To My Work (.*)")
	public HomePagePF GoToMyWork(String filtername) throws InterruptedException
	{
		enterFilterSearch(filtername);
		clickMyWork();
		return this;
	}
	
	@FindBy(how=How.XPATH, using="(//div[text()='My Work'])[1]")
	private WebElement eleClickMyWork;
	
	public MyWorkPage clickMyWork() {
		click(eleClickMyWork);
		defaultContent();
		return new MyWorkPage();		
	}
}

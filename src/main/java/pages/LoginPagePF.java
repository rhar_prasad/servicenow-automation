package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class LoginPagePF extends ProjectSpecificMethods {

	public LoginPagePF() {

		PageFactory.initElements(getDriver(), this);
		switchToFrame("gsft_main");
	}
	
	@FindBy(how=How.ID,using="user_name") 
	private WebElement eleUsername;
	
	public LoginPagePF enterUserName(String username) {
		clearAndType(eleUsername, "admin");
		return this;


	}
	@FindBy(how=How.ID,using="user_password") 
	private WebElement elePassword;
	
	public LoginPagePF enterpassword (String password) {
		clearAndType(elePassword, "India@123");
		defaultContent();
		return this;

	}
	@FindBy(how=How.XPATH,using="//button[contains(@class,'pull-right btn')]") 
	private WebElement eleLoginButton;
	
	public HomePagePF clickLoginButton() {
		click(eleLoginButton);
		System.out.println("Login Successfully");
		return new HomePagePF() ;

	}

}
